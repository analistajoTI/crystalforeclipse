package com.businessobjects.samples;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SapDB {
	
	public static String ip;
	public static String port;
	public static String schemaDB;
	public static String userDB;
	public static String password;

	private static Connection connection() throws SQLException, ClassNotFoundException {
		Class.forName("com.sap.db.jdbc.Driver");
		Connection connection = null;
		if (schemaDB != null) {
			connection = DriverManager.getConnection(
					"jdbc:sap://" + ip + ":" + port + "/?currentschema=" + schemaDB + "&autocommit=false", userDB,
					password);
		} else {
			connection = DriverManager.getConnection("jdbc:sap://" + ip + ":" + port + "/?autocommit=false", userDB,
					password);
		}
		return connection;
	}
	
	public static double getVatPrcnt(Integer docEntry, String Item) throws SQLException, ClassNotFoundException {
		Connection conn = connection();
			String query = "select TOP 1 T1.\"VatPrcnt\" "
							+ "from \"ORDR\" T0 "
							+ "INNER JOIN \"RDR1\" T1 ON T0.\"DocEntry\" = T1.\"DocEntry\" "
							+ "WHERE T0.\"DocNum\" = "+docEntry+" "
							+ "and T1.\"ItemCode\" = '"+Item+"';";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			double vat = 0.0;
			if (rs.isBeforeFirst()) {
				rs.next();
				vat = rs.getDouble("VatPrcnt");
			}
			rs.close();
			stmt.close();
			conn.close();
			return vat;

	}

}
